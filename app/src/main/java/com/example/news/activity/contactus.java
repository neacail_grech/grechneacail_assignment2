package com.example.news.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.news.R;


public class contactus extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private DrawerLayout mdrawerlayout;
    private ActionBarDrawerToggle mToggle;
    EditText dateinput;
    EditText timeinput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactus);
        mdrawerlayout = findViewById(R.id.contact_us);
        mToggle = new ActionBarDrawerToggle(this,mdrawerlayout,R.string.open,R.string.close);
        mdrawerlayout.addDrawerListener(mToggle);
        mToggle.syncState();;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = (NavigationView)findViewById(R.id.navmenu);
        navigationView.setNavigationItemSelectedListener(this);
        dateinput = findViewById(R.id.datetext);
        timeinput = findViewById(R.id.timetext);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }
        if(item.getItemId()==R.id.homenews){
            Intent i = new Intent(this,newshomepage.class);
            this.startActivity(i);
            return true;
        }
        if(item.getItemId() == R.id.newssport){
            Intent i = new Intent(this,sportshomepage.class);
            this.startActivity(i);
            return true;
        }
        if(item.getItemId() == R.id.contact){
            Intent i = new Intent(this, contactus.class);
            this.startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.homenews){
            Intent i = new Intent(this,newshomepage.class);
            this.startActivity(i);
        }
        if(id == R.id.sports){
            Intent i = new Intent(this,sportshomepage.class);
            this.startActivity(i);
        }
        if(id == R.id.contactus){
            Intent i = new Intent(this, contactus.class);
            this.startActivity(i);
        }
        return false;
    }

    @Override
    protected void onPause(){
        super.onPause();
        SharedPreferences settings = getSharedPreferences("info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("date",dateinput.getText().toString());
        editor.putString("time",timeinput.getText().toString());
        editor.commit();
    }
    @Override
    protected void onStop(){
        super.onStop();
        SharedPreferences settings = getSharedPreferences("info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("date",dateinput.getText().toString());
        editor.putString("time",timeinput.getText().toString());
        editor.commit();
    }
    protected void onResume(){
        super.onResume();
        SharedPreferences sharedPref = getSharedPreferences("info", Context.MODE_PRIVATE);
        String date = sharedPref.getString("date","");
        String time = sharedPref.getString("time","");
        dateinput.setText(date);
        timeinput.setText(time);
    }
    @Override
    protected void onStart(){
        super.onStart();
        SharedPreferences sharedPref = getSharedPreferences("info", Context.MODE_PRIVATE);
        String date = sharedPref.getString("date","");
        String time = sharedPref.getString("time","");
        dateinput.setText(date);
        timeinput.setText(time);
    }
    public void saveinfo(View view){
        SharedPreferences sharedPref = getSharedPreferences("info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("date",dateinput.getText().toString());
        editor.putString("time",timeinput.getText().toString());
        editor.apply();
        Toast.makeText(this,"saved",Toast.LENGTH_LONG).show();
    }

    public void displayData(View view){
        SharedPreferences sharedPref = getSharedPreferences("info", Context.MODE_PRIVATE);
        String date = sharedPref.getString("date","");
        String time = sharedPref.getString("time","");
        String dateandtime = date + " "+time;
        Snackbar.make(view,dateandtime,Snackbar.LENGTH_LONG).setAction("Action",null).show();
    }


}
