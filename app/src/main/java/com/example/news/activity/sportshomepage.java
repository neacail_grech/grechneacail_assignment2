package com.example.news.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.example.news.R;
import com.example.news.Utils.Utils;
import com.example.news.adapters.Adapter;
import com.example.news.api.ApiClient;
import com.example.news.api.ApiInterface;
import com.example.news.model.Article;
import com.example.news.model.News;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class sportshomepage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout mdrawerlayout;
    private ActionBarDrawerToggle mToggle;
    public static final String API_KEY = "8df8a7a339694cf68f660aefd2e5ba76";
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<Article> articles = new ArrayList<>();
    private Adapter adapter;
    private String TAG = newshomepage.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newshomepage);
        mdrawerlayout = findViewById(R.id.activity_newshomepage);
        mToggle = new ActionBarDrawerToggle(this,mdrawerlayout,R.string.open,R.string.close);
        mdrawerlayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = findViewById(R.id.recyclerview);
        layoutManager = new LinearLayoutManager(sportshomepage.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        NavigationView navigationView = (NavigationView)findViewById(R.id.navmenu);
        navigationView.setNavigationItemSelectedListener(this);
        LoadJson();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }
        if(item.getItemId()==R.id.homenews){
            Intent i = new Intent(this,newshomepage.class);
            this.startActivity(i);
            return true;
        }
        if(item.getItemId() == R.id.newssport){
            Intent i = new Intent(this,sportshomepage.class);
            this.startActivity(i);
            return true;
        }
        if(item.getItemId() == R.id.contact){
            Intent i = new Intent(this, contactus.class);
            this.startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.homenews){
            Intent i = new Intent(this,newshomepage.class);
            this.startActivity(i);
        }
        if(id == R.id.sports){
            Intent i = new Intent(this,sportshomepage.class);
            this.startActivity(i);
        }
        if(id == R.id.contactus){
            Intent i = new Intent(this, contactus.class);
            this.startActivity(i);
        }
        return false;
    }
    public void LoadJson(){
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        String country = Utils.getCountry();
        Call<News> call;
        call = apiInterface.getNews(country,"sports",API_KEY);
        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                if (response.isSuccessful() && response.body().getArticle() != null) {
                    if (!articles.isEmpty()) {
                        articles.clear();
                    }
                    articles = response.body().getArticle();
                    adapter = new Adapter(articles, sportshomepage.this);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    initListener();
                } else {
                    Toast.makeText(sportshomepage.this, "No Response!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<News> call, Throwable t) {
            }
        });
    }
    private void initListener(){
        adapter.setOnItemClickListener(new Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(sportshomepage.this,NewsOverlay.class);
                Article article = articles.get(position);
                intent.putExtra("url",article.getUrl());
                intent.putExtra("title",article.getTitle());
                intent.putExtra("img",article.getUrlToImage());
                intent.putExtra("author",article.getAuthor());
                intent.putExtra("source",article.getSource().getName());
                intent.putExtra("date",article.getPublishedAt());
                intent.putExtra("desc",article.getDescription());

                startActivity(intent);
            }
        });
    }
}
