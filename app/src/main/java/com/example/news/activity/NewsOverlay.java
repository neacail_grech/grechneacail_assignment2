package com.example.news.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.example.news.R;
import com.example.news.Utils.Utils;

public class NewsOverlay extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {
    private ImageView imageView;
    private TextView appbar_title,date, time, title,description;
    private boolean ishidetoolbarview = false;
    private FrameLayout datebehaviour;
    private LinearLayout titleAppbar;
    private AppBarLayout appbarlayout;
    private Toolbar toolbar;
    private String murl, mimg, mtitle, mdate, msource, mauthor,mdesc;
    private Button mbutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_overlay);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle("");
        mbutton = findViewById(R.id.url);
        appbarlayout = findViewById(R.id.appbar);
        appbarlayout.addOnOffsetChangedListener(this);
        datebehaviour = findViewById(R.id.date_behavior);
        titleAppbar = findViewById(R.id.title_appbar);
        appbar_title = findViewById(R.id.title_on_appbar);
        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        title = findViewById(R.id.title);
        imageView = findViewById(R.id.backdrop);
        description = findViewById(R.id.content);
        Intent intent = getIntent();
        murl = intent.getStringExtra("url");
        mimg = intent.getStringExtra("img");
        mtitle = intent.getStringExtra("title");
        mdate = intent.getStringExtra("date");
        msource = intent.getStringExtra("source");
        mauthor = intent.getStringExtra("author");
        mdesc = intent.getStringExtra("desc");
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(Utils.getRandomDrawbleColor());
        Glide.with(this).load(mimg).apply(requestOptions).transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView);
        appbar_title.setText(msource);
        date.setText(Utils.DateFormat(mdate));
        title.setText(mtitle);
        description.setText(mdesc);
        String author = null;
        if(mauthor != null || mauthor !=""){
            mauthor="\u2022"+mauthor;
        }else{
            author= "";
        }
        time.setText(msource+author+" \u2022 "+Utils.DateToTimeFormat(mdate));
        mbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(murl));
                startActivity(intent);
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        supportFinishAfterTransition();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int maxScroll = appbarlayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;

        if (percentage == 1f && ishidetoolbarview) {
            datebehaviour.setVisibility(View.GONE);
            titleAppbar.setVisibility(View.VISIBLE);
            ishidetoolbarview = !ishidetoolbarview;
        } else if (percentage < 1f && !ishidetoolbarview) {
            datebehaviour.setVisibility(View.VISIBLE);
            titleAppbar.setVisibility(View.GONE);
            ishidetoolbarview = !ishidetoolbarview;
        }
    }
}
